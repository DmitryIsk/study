def calculate():
    POPULATION = 1500000
    PERCENT = 70
    ILL = POPULATION * (PERCENT/100)
    # ILL = 935969
    Q = 1.121
    START = 10
    # EXPECT_DAYS = 71

    days = 0
    while START <= ILL:
        START = START*Q
        days += 1
    
    print(days)

if __name__ == "__main__":
    calculate()