# Given three integers a ,b ,c, return the largest number obtained after inserting the following operators and brackets: +, *, ()
# In other words , try every combination of a,b,c with [*+()] , and return the Maximum Obtained

def expression_matter(a, b, c):
    spisok = [
        a*b*c,
        a*(b+c),
        (a+b)*c,
        a+b+c
    ]
    m = max(spisok)
    return m


if __name__ == "__main__":
    expression_matter(1, 3, 1)