import random

class Box:
    def __init__(self, height, width, length, weight):
        self.height = height
        self.width = width
        self.length = length
        self.weight = weight

    def box_volume(self):
        return self.height * self.width * self.length

class Car:
    height = random.randint(2500, 3500)        # ~3050 mm
    width = random.randint(2500, 3000)         # ~2764 mm
    length = random.randint(12000, 17000)      # ~15724 mm
    weight_max = random.randint(65000, 75000)  # ~70000 kg
    box_list = []
    weight_now = 0

    def car_volume(self):
        return self.height * self.width * self.length

    def size_in(self):
        if (Car.height, Car.width, Car.length) >= (box.height, box.width, box.length):
            return True

    def weight_in(self):
        if Car.weight_max >= Car.weight_now + box.weight:
            return True
    
    def volume_in(self, free_volume):
        if free_volume >= free_volume - box.box_volume():
            return True


if __name__ == "__main__":
    car = Car()
    free_volume = car.car_volume()
    fail_list = []
    print('CAR = ', car.height, car.width, car.length, car.weight_max)

    for i in range(10):
        box = Box(
            random.randint(1000, 4000),
            random.randint(1000, 4000),
            random.randint(1000, 20000),
            random.randint(1000, 20000),
        )

        if car.size_in() and car.weight_in() and car.volume_in(free_volume):
            Car.weight_now += box.weight
            Car.box_list.append(box.__dict__)
        else:
            fail_list.append(box.__dict__)

    for a in Car.box_list:
        print('BOX_IN = ', a)
    for b in fail_list:
        print('BOX_OUT = ', b)
