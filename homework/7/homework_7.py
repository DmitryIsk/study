# Домашнее задание №7
# Продолжаем работать с вагонами (класс Carriage) и грузами (классы: Cargo, Box, Fridge, Table).
# В этом задании нужно:
# 1) создать класс поезд - Train. У которого будет: 
# - номер: number
# - координаты: x, y и метод "переместить" - выносим из класса Carriage
# - список вагонов: сarriage_list
# - метод "добавить вагон в конец"
# - метод "отцепить последний вагон"
# 2) создать поезд и добавить в него заданное количество вагонов (константа)
# 3) для каждого типа грузов создать свою фабрику: BoxFactory, FridgeFactory, TableFactory, Задача фабрики создать обьект груза. 
# Выносим классметод, который определял рандомные параметры груза, из класса Cargo в класс фабрики.
# 4) создаем генератор, который будет бесконечно (пока его вызывают) создавать рандомный груз
# 5) организовать заполнение всех вагонов поезда грузами полученными из генератора. 
# Заполнение вагона происходит пока новый товар можно поместить в вагон. 
# Как только товар не помещается в вагон, переходим к следующему вагону и так пока не заполнятся все вагоны. 
# Последний товар, который никуда не поместился, выбрасываем (del cardo)


import random


class Train:
    def __init__(self, number, x, y, сarriage_list):
        self.n = number
        self.x = x
        self.y = y
        self.сarriage_list = сarriage_list

    def add_carriege_to_end(self, carriege):
        self.сarriage_list.append(carriege)

    def remove_last_carriege(self):
        self.сarriage_list.pop()

    def move_train(self, x_add, y_add):
        self.x += x_add
        self.y += y_add

    def create_train(self, const):
        for i in range(const):
            self.сarriage_list.append(i)


class Cargo:
    def __init__(self, h, w, l, weight):
        self.h = h
        self.w = w
        self.l = l
        self.weight = weight
        self.volume = self.h * self.w * self.l


class Box(Cargo):
    def __str__(self):
        return f'Box: \th={self.h} \tw={self.w} \tl={self.l} \tweight={self.weight}'


class Fridge(Cargo):
    def __str__(self):
        return f'Fridge: \th={self.h} \tw={self.w} \tl={self.l} \tweight={self.weight}'


class Table(Cargo):
    def __str__(self):
        return f'Table: \th={self.h} \tw={self.w} \tl={self.l} \tweight={self.weight}'


class Carriage:
    h = random.randint(2500, 3500)
    w = random.randint(2500, 3000)
    l = random.randint(12000, 17000)
    weight_max = random.randint(65000, 75000)
    free_volume = h * w * l
    box_list = []
    weight_now = 0
    open = random.choice([True, False])
    ready = random.choice([True, False])

    def check_cargo(self, cargo):
        if (self.h, self.w, self.l) >= (cargo.h, cargo.w, cargo.l):
            if self.weight_max >= self.weight_now + cargo.weight:
                if self.free_volume - cargo.volume >= 0:
                    return True
        else:
            return False

    def add_cargo(self, cargo):
        self.box_list.append(cargo.__dict__)
        self.weight_now += cargo.weight
        self.free_volume -= cargo.volume

    def get_last_cargo(self):
        if len(self.box_list) > 0:
            return self.box_list[-1]
        else:
            return "Empty carriege!"

    def __str__(self):
        print(f'CARRIEGE: \th={self.h} \tw={self.w} \tl={self.l} weight_max={self.weight_max}')


if __name__ == "__main__":
    cargo_classes = [Box, Fridge, Table]

    carriege = Carriage()
    carriege.__str__()

    for i in range(10):
        cargo = random.choice(cargo_classes)(
            random.randint(1000, 4000),
            random.randint(1000, 4000),
            random.randint(1000, 9000),
            random.randint(1000, 9000),
        )

        if carriege.check_cargo(cargo):
            carriege.add_cargo(cargo)
            print('add ' + cargo.__str__())
        else:
            print('out ' + cargo.__str__())
