import os

def to_lowercase(l):
    return l.lower()

def double_spaces_replacement(l):
    return l.replace("  ", " ")

def remove_dots_commas(l):
    return l.replace(".", "").replace(",", "")

def main_function():
    path = os.path.dirname(__file__)
    with open(path + '/data.txt', "r") as f:
        with open(path + '/output.txt', "w") as fo:
            for line in f:
                fo.write(
                    remove_dots_commas(
                        double_spaces_replacement(
                            to_lowercase(line)
                        )
                    )
                )

if __name__ == "__main__":
    main_function()
