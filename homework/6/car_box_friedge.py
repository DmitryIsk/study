import random

class Cargo:
    def __init__(self, h, w, l, weight):
        self._h = h
        self._w = w
        self._l = l
        self._weight = weight
        self.volume = self._h * self._w * self._l

    @property
    def h(self):
        return self._h

    @property
    def w(self):
        return self._w

    @property
    def l(self):
        return self._l

    @property
    def weight(self):
        return self._weight


class Box(Cargo):
    def __str__(self):
        return f'Box: \th={self.h} \tw={self.w} \tl={self.l} \tweight={self.weight}'


class Fridge(Cargo):
    def __str__(self):
        return f'Fridge: \th={self.h} \tw={self.w} \tl={self.l} \tweight={self.weight}'


class Table(Cargo):
    def __str__(self):
        return f'Table: \th={self.h} \tw={self.w} \tl={self.l} \tweight={self.weight}'


class Carriage:
    def __init__(
        self,
        h,
        w,
        l,
        weight_max,
        box_list,
        weight_now,
        open,
        ready,
        x,
        y,
    ):
        self._h = h                     # random.randint(2500, 3500)        # ~3050 mm
        self._w = w                     # random.randint(2500, 3000)        # ~2764 mm
        self._l = l                     # random.randint(12000, 17000)      # ~15724 mm
        self._weight_max = weight_max   # random.randint(65000, 75000)      # ~70000 kg
        self.free_volume = self._h * self._w * self._l
        self.box_list = box_list
        self.weight_now = weight_now
        self.open = open
        self.ready = ready
        self.x = x
        self.y = y
    
    @property
    def h(self):
        return self._h

    @property
    def w(self):
        return self._w

    @property
    def l(self):
        return self._l

    @property
    def weight_max(self):
        return self._weight_max

    def check_cargo(self, cargo):
        if (self.h, self.w, self.l) >= (cargo.h, cargo.w, cargo.l):
            if self.weight_max >= self.weight_now + cargo.weight:
                if self.free_volume - cargo.volume >= 0:
                    return True
        else:
            return False
    
    def check_carriege(self, carriege):
        if self.open:
            carriege.move_carriege(self.x, self.y) # move to (0, 0) for cargo add
            return True
        else:
            self.open = True
            carriege.check_carriege(carriege)

    def add_cargo(self, cargo):
        self.box_list.append(cargo.__dict__)
        self.weight_now += cargo.weight
        self.free_volume -= cargo.volume

    def move_carriege(self, x_add, y_add): # add in (0, 0)
        self.x += x_add
        self.y += y_add

    def get_last_cargo(self):
        if len(self.box_list) > 0:
            return self.box_list[-1]
        else:
            return "Empty carriege!"

    def __str__(self):
        print(f'CARRIEGE: \th={self.h} \tw={self.w} \tl={self.l} weight_max={self.weight_max}')

if __name__ == "__main__":
    cargo_classes = [Box, Fridge, Table]

    carriege = Carriage(
        random.randint(2500, 3500),
        random.randint(2500, 3000),
        random.randint(12000, 17000),
        random.randint(65000, 75000),
        [],
        0,
        random.choice([True, False]),
        random.choice([True, False]),
        random.uniform(0, 100),
        random.uniform(0, 100),
    )
    carriege.__str__()

    for i in range(10):
        cargo = random.choice(cargo_classes)(
            random.randint(1000, 4000),
            random.randint(1000, 4000),
            random.randint(1000, 9000),
            random.randint(1000, 9000),
        )
        if carriege.check_cargo(cargo) and carriege.check_carriege(carriege):
            carriege.add_cargo(cargo)
            print('add ' + cargo.__str__())
        else:
            print('out ' + cargo.__str__())
