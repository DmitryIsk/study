import random
import timeit

def generate_stack_n_needle(number, stack_type):
    # generate stack
    stack_dict = dict((random.random(), 1) for k in range(number))
    # generate needle
    keys_for_needle_dict = [k for k in random.sample(stack_dict.keys(), 500)] + [k for k in range(500)]
    random.shuffle(keys_for_needle_dict)
    needle_dict = dict((k, 1) for k in keys_for_needle_dict)

    if str(stack_type) == "<class 'dict'>":
        return stack_dict, needle_dict

    if str(stack_type) == "<class 'list'>":
        stack_list = [stack_dict.keys()]
        needle_list = keys_for_needle_dict
        return stack_list, needle_list

    if str(stack_type) == "<class 'tuple'>":
        stack_tuple = tuple(stack_dict.keys())
        needle_tuple = tuple(keys_for_needle_dict)
        return stack_tuple, needle_tuple

def found_and_time(stack, needle):
    time_start = timeit.default_timer()
    for i in needle:
        if i in stack:
            pass
    delta = timeit.default_timer() - time_start
    return delta

def main_function(number, stack_type):
    s, n = generate_stack_n_needle(number, stack_type)
    print('TYPE =', stack_type, 'LEN =', number, 'TIME =', found_and_time(s, n))

if __name__ == "__main__":
    max_line = 100
    num_of_elements = [1000, 10000, 100000, 1000000]
    type_list = [list, tuple, dict]
    
    for t in type_list:
        print(max_line*'=')
        for n in num_of_elements:
            main_function(n, t)
