# Домашнее задание N8
# Создать класс для представления трехмерных векторов (обычных евклидовых). 
# С помощью специальных методов: "__add__", "__mul__", "__abs__", "__bool__", "__str__" - определить сложение векторов, 
# умножение вектора на число, длинна вектора, булево значение (True - если длинна > 0) и строковое представление объекта

import math
import const
import random

class Vector:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __add__(self, sec_vector):
        x_summ = self.x + sec_vector.x
        y_summ = self.y + sec_vector.y
        z_summ = self.z + sec_vector.z
        return x_summ, y_summ, z_summ
        
    def __mul__(self, const):
        return self.x*const, self.y*const, self.z*const

    def __abs__(self):
        return math.sqrt(self.x**2 + self.y**2 + self.z**2)

    def __bool__(self):
        return self.x != 0 or self.y != 0 or self.z != 0

    def __str__(self):
        return f"({self.x}, {self.y}, {self.z})"

def create_vector():
    vector = Vector(
        random.randint(-10, 10),
        random.randint(-10, 10),
        random.randint(-10, 10)
    )
    return vector

if __name__ == "__main__":
    constant = random.randint(-10, 10)
    v1 = create_vector()
    v2 = create_vector()
    
    print(f'V1 = {v1} \nV2 = {v2} \nCONST = {constant}\n')
    print(f'SUMM = {v1 + v2}')
    print(f'MULT = {v1 * constant}')
    print(f'ABS = {abs(v1)}')
    print(f'BOOL = {bool(v1)}')
